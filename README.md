# README #

### How does it work? ###
The program works by modifying the maps .pak file name so the game can't find the file, therefore it can't load it.

### What happens when the game can't load the map file ###
You will get kicked almost immediately back to the lobby.

### Is this safe? ###
Even though this program simply modifys the name of a .pak file I can't confirm this is safe
USE AT YOUR OWN RISK.


