﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MapSelection.IO;
using MapSelection.Maps;

namespace MapSelection
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Save the current path.
        /// </summary>
        private void SavePath()
        {
            if (!String.IsNullOrEmpty(Properties.Settings.Default.GamePath))
                MessageBox.Show("Updated target path", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

            Properties.Settings.Default.GamePath = Disk.GamePath;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Update the GUI.
        /// </summary>
        private void UpdateGUI()
        {
            labelPath.Text = Disk.GamePath;

            cbErangel.Checked = !Disk.MapDisabled(Map.erangel);
            cbDesert.Checked = !Disk.MapDisabled(Map.desert);
        }

        /// <summary>
        /// Display a popup for a duplicate error and return the end-user result.
        /// </summary>
        /// <param name="mapName"></param>
        /// <returns></returns>
        private bool DuplicatePopup(string mapName)
        {
            return MessageBox.Show("It would appear you have a duplicate .pak file, this was most likely caused by a game update. " +
                "Would you like to delete this duplicate? NOTE: If you choose NO the program will exit.", 
                $"{mapName} .pak duplicate", 
                MessageBoxButtons.YesNo) == DialogResult.Yes;
        }

        /// <summary>
        /// Check if the game updated and caused duplicate files to be created.
        /// </summary>
        private void CheckForDuplicates()
        {
            //Check erangel.
            if (Disk.GameUpdated(Map.erangel))
            {
                if(DuplicatePopup(Map.erangel.ToString()))
                {
                    Disk.DeleteDuplicate(Map.erangel);
                }
                else
                {
                    Environment.Exit(0);
                }
              
            }

            //Check desert.
            if (Disk.GameUpdated(Map.desert))
            {
                if(DuplicatePopup(Map.desert.ToString()))
                {
                    Disk.DeleteDuplicate(Map.desert);
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }


        private void frmMain_Load(object sender, EventArgs e)
        {
            string tempPath;
            if ((tempPath = Properties.Settings.Default.GamePath) != null)
            {
                Disk.GamePath = tempPath;
                CheckForDuplicates();
                UpdateGUI();
            }

            

        }

        private void btnSelectPath_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                Disk.GamePath = folderBrowserDialog.SelectedPath + "\\";

                SavePath();

                CheckForDuplicates();

                UpdateGUI();
            }
        }

        private void btnFindPath_Click(object sender, EventArgs e)
        {
            string tempPath;

            if((tempPath = Disk.TryFindGamePath()) != null)
            {
                Disk.GamePath = tempPath;

                SavePath();

                UpdateGUI();
            }
            else
            {
                MessageBox.Show("Failed to automatically find game path. Are you sure you have the target game installed?", 
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if(Disk.GamePath == string.Empty)
            {
                MessageBox.Show("Please select the path first!");
                return;
            }

            Disk.EditMap(Map.erangel, cbErangel.Checked);
            Disk.EditMap(Map.desert, cbDesert.Checked);
        }
    }
}
