﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MapSelection.Maps;

namespace MapSelection.IO
{
    public static class Disk
    {

        public static string GamePath
        {
            get; set;
        } = string.Empty;


        #region Prefixs

        private static readonly string startPrefix = "TslGame-WindowsNoEditor_";

        private static readonly string endPrefix = ".pak";

        #endregion

        #region Private Methods

        /// <summary>
        /// Rename the target file with a new name.
        /// </summary>
        /// <param name="targetPath">target path.</param>
        /// <param name="newName">new name</param>
        private static void Rename(string targetPath, string newName)
        {
            File.Move(targetPath, Path.Combine(targetPath, newName));
        }

        /// <summary>
        /// Return the default (enabled path) for the map.
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        private static string DefaultMapPath(Map map)
        {
            return $"{GamePath}{startPrefix}{map.ToString()}{endPrefix}";
        }

        /// <summary>
        /// Return the current map path, disabled or enabled.
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        private static string CurrentMapPath(Map map)
        {
            bool disabled = MapDisabled(map);

            string path = DefaultMapPath(map);

            return disabled ? $"{path}.disabled" : path;
        }

        /// <summary>
        /// Returns true if the 'map' is disabled.
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static bool MapDisabled(Map map)
        {
            return !File.Exists(DefaultMapPath(map));
        }

        /// <summary>
        /// Prevent the game from loading the map.
        /// </summary>
        /// <param name="map"></param>
        private static void DisableMap(Map map)
        {
            if (MapDisabled(map))
                return;

            string mapPath = CurrentMapPath(map);

            Rename(mapPath, $"{mapPath}.disabled");
        }

        /// <summary>
        /// Allow the game to load the map.
        /// </summary>
        /// <param name="map"></param>
        private static void EnableMap(Map map)
        {
            if (!MapDisabled(map))
                return;

            string mapPath = CurrentMapPath(map);

            Rename(mapPath, mapPath.Replace(".disabled", string.Empty));
        }

        #endregion

        #region Public Methods

        public static bool GameUpdated(Map map)
        {
            return File.Exists(DefaultMapPath(map)) && File.Exists($"{DefaultMapPath(map)}.disabled");
        }

        public static void DeleteDuplicate(Map map)
        {
            File.Delete(DefaultMapPath(map));
        }

        /// <summary>
        /// Try to automatically find the target game path. 
        /// </summary>
        /// <returns>Returns null if path was not found.</returns>
        public static string TryFindGamePath()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();

            foreach (DriveInfo dInfo in drives)
            {
                string tempPath = Path.Combine(dInfo.Name, @"Steam\steamapps\common\PUBG\TslGame\Content\Paks\");

                if (Directory.Exists(tempPath))
                    return tempPath;
            }

            return null;
        }

        /// <summary>
        /// Enable or disable the map.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="allow"></param>
        public static void EditMap(Map map, bool allow)
        {
            if(allow)
            {
                EnableMap(map);
            }
            else
            {
                DisableMap(map);
            }
        }

        #endregion

    }
}
