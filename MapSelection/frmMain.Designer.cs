﻿namespace MapSelection
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelectPath = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.cbErangel = new System.Windows.Forms.CheckBox();
            this.cbDesert = new System.Windows.Forms.CheckBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnFindPath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPath = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSelectPath
            // 
            this.btnSelectPath.Location = new System.Drawing.Point(18, 26);
            this.btnSelectPath.Name = "btnSelectPath";
            this.btnSelectPath.Size = new System.Drawing.Size(153, 23);
            this.btnSelectPath.TabIndex = 0;
            this.btnSelectPath.Text = "Select Paks Folder";
            this.btnSelectPath.UseVisualStyleBackColor = true;
            this.btnSelectPath.Click += new System.EventHandler(this.btnSelectPath_Click);
            // 
            // cbErangel
            // 
            this.cbErangel.AutoSize = true;
            this.cbErangel.Location = new System.Drawing.Point(18, 69);
            this.cbErangel.Name = "cbErangel";
            this.cbErangel.Size = new System.Drawing.Size(90, 17);
            this.cbErangel.TabIndex = 1;
            this.cbErangel.Text = "Allow Erangel";
            this.cbErangel.UseVisualStyleBackColor = true;
            // 
            // cbDesert
            // 
            this.cbDesert.AutoSize = true;
            this.cbDesert.Location = new System.Drawing.Point(242, 69);
            this.cbDesert.Name = "cbDesert";
            this.cbDesert.Size = new System.Drawing.Size(85, 17);
            this.cbDesert.TabIndex = 2;
            this.cbDesert.Text = "Allow Desert";
            this.cbDesert.UseVisualStyleBackColor = true;
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(18, 97);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(309, 23);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "Apply Options";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnFindPath
            // 
            this.btnFindPath.Location = new System.Drawing.Point(181, 26);
            this.btnFindPath.Name = "btnFindPath";
            this.btnFindPath.Size = new System.Drawing.Size(153, 23);
            this.btnFindPath.TabIndex = 4;
            this.btnFindPath.Text = "Auto Find Paks Folder";
            this.btnFindPath.UseVisualStyleBackColor = true;
            this.btnFindPath.Click += new System.EventHandler(this.btnFindPath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Path:";
            // 
            // labelPath
            // 
            this.labelPath.AutoSize = true;
            this.labelPath.Location = new System.Drawing.Point(29, 3);
            this.labelPath.Name = "labelPath";
            this.labelPath.Size = new System.Drawing.Size(62, 13);
            this.labelPath.TabIndex = 6;
            this.labelPath.Text = "placeholder";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 132);
            this.Controls.Add(this.labelPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFindPath);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.cbDesert);
            this.Controls.Add(this.cbErangel);
            this.Controls.Add(this.btnSelectPath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMain";
            this.Text = "PUBG Map Selection Tool";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelectPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.CheckBox cbErangel;
        private System.Windows.Forms.CheckBox cbDesert;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnFindPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelPath;
    }
}

